#Include header.ahk
#IfWinActive, Buscar Colaboradores
Gui, +lastFound
hWnd := WinExist()
hSysMenu:=DllCall("GetSystemMenu","Int",hWnd,"Int",FALSE) 
nCnt:=DllCall("GetMenuItemCount","Int",hSysMenu) 
DllCall("RemoveMenu","Int",hSysMenu,"UInt",nCnt-6,"Uint","0x400") 
DllCall("DrawMenuBar","Int",hWnd) 
;{
Gui, Add, Edit, x5 y5 w150 h20  vmat
Gui, Add, Button, x155 y5 w300 h20  gsubmit, Buscar por Matrícula, Nome, Ramal, Cargo, Setor ou Unidade
Gui, Add, ListView, x5 y35 w1270 h260 vlv, Matrícula|Nome|Telefone 1| Telefone 2|Ramal|Cargo|Setor|Local
Gui, Show, x0 y0 w1280 h310, Buscar Colaboradores
return ;}

GuiClose: ;{
ExitApp ;}

submit: ;{
GuiControl, Disable, mat
Gui, Submit, NoHide
StringUpper, mat, mat
matz := StrSplit(mat,A_Space,A_Space)
matz1 := matz[1]
matz2 := matz[2]
if ( mat = "" )
{
 GuiControl, Enable, mat
 GuiControl, Focus, mat
 return
}
LV_Delete()
q_ddl =
(
with first as ( select NUMCAD, NM_RAZAO_SOCIAL, FONE, CELULAR, RAMAL, DN_CARGO, DN_SETOR, DN_LOCAL 
FROM CAD_FUNCIONARIOS
WHERE ( NUMCAD like '%matz1%`%' or NM_RAZAO_SOCIAL like '`%%matz1%`%' or DN_CARGO like '`%%matz1%`%' or DN_SETOR like '`%%matz1%`%' or DN_LOCAL like '`%%matz1%`%' or RAMAL like '%matz1%`%' )
AND SITUACAO = '1' )

select NUMCAD, NM_RAZAO_SOCIAL, FONE, CELULAR, RAMAL, DN_CARGO, DN_SETOR, DN_LOCAL 
FROM first
WHERE ( NUMCAD like '%matz2%`%' or NM_RAZAO_SOCIAL like '`%%matz2%`%' or DN_CARGO like '`%%matz2%`%' or DN_SETOR like '`%%matz2%`%' or DN_LOCAL like '`%%matz2%`%' or RAMAL like '`%%matz2%`%' ) 
ORDER BY 2
)
Table :=
Table := ADOSQL(ora, q_ddl)
Loop % Table.MaxIndex() -1
{
col := A_Index+1
name := Table[col, 2] 
tel1 := Table[col, 3] 
tel2 := Table[col, 4] 
cargo := Table[col, 6] 
setor := Table[col, 7] 
unidade := Table[col, 8] 
t1 := StrLen(tel1)
t2 := StrLen(tel2)
if ( t1 = 10 )  ;{
{
 StringTrimLeft, ty3, tel1, 6
 StringTrimRight, ty2, tel1, 4
 StringTrimRight, ty1, tel1, 8
 StringTrimLeft, ty2, ty2, 2
 tel1 := "(" ty1 ")   " ty2 " - " ty3
}
if ( t1 = 11 )
{
 StringTrimLeft, ty3, tel1,7
 StringTrimRight, ty2, tel1, 4
 StringTrimRight, ty1, tel1, 9
 StringTrimLeft, ty2, ty2, 2
 tel1 := "(" ty1 ") " ty2 " - " ty3
}
if ( t2 = 10 )
{
 StringTrimLeft, ty3, tel2, 6
 StringTrimRight, ty2, tel2, 4
 StringTrimRight, ty1, tel2, 8
 StringTrimLeft, ty2, ty2, 2
 tel2 := "(" ty1 ")   " ty2 " - " ty3
}
if ( t2 = 11 )
{
 StringTrimLeft, ty3, tel2, 7
 StringTrimRight, ty2, tel2, 4
 StringTrimRight, ty1, tel2, 9
 StringTrimLeft, ty2, ty2, 2
 tel2 := "(" ty1 ") " ty2 " - " ty3
} ;}
StringUpper, cname, name, T
StringUpper, ccargo, cargo, T
StringUpper, csetor, setor, T
StringUpper, cuni, unidade, T
LV_Add("", Table[col, 1] ,  cname , tel1 , tel2 , Table[col, 5] , ccargo,csetor,cuni)
}
GuiControl, , lv
GuiControl, , mat
 GuiControl, Enable, mat
GuiControl, Focus, mat
LV_ModifyCol()
LV_ModifyCol(5,75)
LV_ModifyCol(1,75)
return ;}

$Enter:: ;{
$NumpadEnter::
gosub submit
return ;}

Sistema: ;{
Process, Exist, DDguard Player.exe
if !Errorlevel 
 ExitApp
 return
 ;}
