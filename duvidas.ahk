#Include header.ahk
#SingleInstance Force
Gui, -Caption
Gui, Margin, 0, 0
Gui, Add, Text, x4 y12 w100 h20 , Selecionar Sistema
Gui, Add, DropDownList, x105 y10 w230 h20 R5 gDDL vDDL , D-Guard|Iris|Sistema Monitoramento|Procedimentos
Gui, Add, Edit, x4 y40 w330 h130 vDuvida , 
Gui, Add, Button, x4 y180 w160 h30 gAdicionar , Adicionar D�vida
Gui, Add, Button, x174 y180 w160 h30 gGuiClose , Cancelar
Gui, Show, w342 h221 , Adicionar D�vidas ao Banco de Dados
return

DDL: ;{
Gui, Submit, NoHide
GuiControl, Focus, Duvida
Sistema := DDL
return ;}

Adicionar: ;{
FormatTime, HPergunta, DD/MM/YYYY HH24:MI:SS
WinGetTitle, Operador, Painel de Monitoramento
StringSplit, Operador, Operador, |
Gui, Submit, NoHide
if ( DDL = "" )
{
 MsgBox Voc� precisa selecionar um sistema para adicionar sua d�vida!
 GuiControl, Choose, DDL, 1
 return
}
if ( Duvida = "" )
{
 MsgBox Voc� precisa adicionar sua d�vida no campo edit�vel antes de clicar em adicionar!
 GuiControl, Focus, Duvida
 return
}
insert =
(
 INSERT INTO [Sistema Monitoramento by Dieisson].[dbo].[Duvidas]
 VALUES ( '%DDL%', '%Operador2%', '%Duvida%', '%HPergunta%', '%A_IPAddress1%', '', '', '', '')
)
ADOSQL(Con, insert)
if ( ADOSQL_LastError = "" )
{
 IniWrite, Duvida adicionada na m�quina %A_IpAddress1% as %HPergunta%, \\fs\Departamentos\monitoramento\Monitoramento\Dieisson\SMK\ini\Duvidas Log.ini, %A_IPAddress1%, %HPergunta%
 MsgBox, , ,Duvida adicionada com sucesso!, 2
 ExitApp
}
ExitApp
;}

Sistema: ;{
Process, Exist, DDguard Player.exe
if !Errorlevel 
 ExitApp
 return
 ;}

GuiClose:
ExitApp